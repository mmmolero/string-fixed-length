const string = require('string');

module.exports.build = function(data, length, callback) {
  let result;

  // validate


  // check length
  if (data.length <= length) {
    result = string(data).padRight(length).s;
  }
  else
    result = string(data).left(length).s;

  return callback(null, result);
}